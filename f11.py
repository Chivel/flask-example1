import requests
import datetime

json_bank = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json'

try:
    res = requests.get(json_bank)
except:
    raise ValueError ('страница не доступна')

def foo(rest):
    if rest.headers['Content-Type'] == 'application/json; charset=utf-8' and rest.status_code == 200:
        with open('Json_Bank.txt', 'w') as file:
            date = datetime.date.today()
            re = ''
            file.write('Дата создания запроса:  ' + date.strftime('%d/%m/%Y\n') + '\n')
            j_res = rest.json()
            for index, key in enumerate(j_res):
                re += f'{index + 1}. {key["txt"]} to UAH: {key["rate"]}\n'
            file.write(re)
    else:
        raise Exception

foo(res)
